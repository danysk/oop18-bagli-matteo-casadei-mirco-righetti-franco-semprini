package it.unibo.oop.crossline.drawabale.product;

import java.util.Optional;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;

import it.unibo.oop.crossline.drawabale.AnimationState;

/**
 *
 *class that model an animation,it's uses an .atlas extension file an .png image
 *to assemble an array of images that follow each other for
 *draw an animation on the screen.
 *
 */
public class SkinProductAnimatedImpl implements SkinProduct {


    //Field to store images.

    private TextureAtlas skinAtlas;
    private TextureRegion currentFrameToDraw;
    private Animation<?> skinAnimation;
    private Array<AtlasRegion> arrayFrames;

    //filed to set propriety

    private float elapsedTime;
    private final float frameDuration;
    private final TextureRegion firstTexture;
    private static final float IMAGE_PIXEL = 16;
    private final float scale = Gdx.graphics.getDensity() * IMAGE_PIXEL;
    private final Vector2 dim;
    private AnimationState state;

    /**
     *
     * @param pathAtlas Path of file image.atlas
     * @param nameAnimation name of animation_XXX.png where nameAnimation is animation and XXX is image number frame
     * @param speedAnimation speed with which the images follow each other
     * @param type loop type
     */
    public SkinProductAnimatedImpl(final String pathAtlas, final String nameAnimation, final float speedAnimation, final PlayMode type) {

        super();
        frameDuration = speedAnimation;
        this.createTextureAtlasFromFile(pathAtlas, nameAnimation);
        this.createAnimation(arrayFrames, type);
        firstTexture = arrayFrames.first();
        dim = new Vector2(getWidth(), getHeight());
    }


    /**
     * To call before batch.begin(), switch image to create movement.
     *
     */
    public void swichFrameByTime() {

        //check the limit of memory
        if (elapsedTime > Double.SIZE / 2) {
            elapsedTime = 0f;
        }
        elapsedTime += Gdx.graphics.getDeltaTime();
        createFrameToShow(skinAnimation);
    }

    @Override
    public final void drawOnSpriteBatch(final SpriteBatch batch, final Vector2 pos) {

        final Vector2 correctpos = correctPosition(pos);

        if (Optional.of(currentFrameToDraw).isPresent()) {
            batch.draw(currentFrameToDraw, correctpos.x, correctpos.y, dim.x, dim.y);
        }

    }

     @Override
     public final void dispose() {

          skinAtlas.dispose();
      }

     private Vector2 correctPosition(final Vector2 pos) {

         return new Vector2(pos.x - dim.x / 2, pos.y - dim.y / 2);
     }




    /**
     *
     * @param path of resources packed with "GDX Texture Packer"
     * @param nameAnimation is the name of each frame
     *
     */
    private void createTextureAtlasFromFile(final String path, final String nameAnimation) {

        this.skinAtlas = new TextureAtlas(Gdx.files.internal(path));
        this.arrayFrames = skinAtlas.findRegions(nameAnimation);
    }


    /**
    * Create an frame to show based of elapsed time.
    * @param animation
    *
    */
    private void createFrameToShow(final Animation<?> animation) {

          currentFrameToDraw = (TextureRegion) animation.getKeyFrame(elapsedTime);

    }

    /**
     * Create an animation.
     * @param arrayFrames every image to be loop
     * @param type loop mode
     *
     */
    private void createAnimation(final Array<AtlasRegion> arrayFrames, final PlayMode type) {

        skinAnimation = new Animation<>(frameDuration, arrayFrames, type);
    }


    private float getHeight() {

        return (scale / firstTexture.getRegionHeight());
    }

    private float getWidth() {

        return (scale / firstTexture.getRegionWidth());
    }

    @Override
    public final AnimationState getState() {
        return state;
    }

    @Override
    public final void setState(final AnimationState state) {
        this.state = state;
    }


}
