package it.unibo.oop.crossline.drawabale.product.attributes.idle;

import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;

import it.unibo.oop.crossline.drawabale.AnimationState;
import it.unibo.oop.crossline.drawabale.product.SkinProduct;
import it.unibo.oop.crossline.drawabale.product.SkinProductAnimatedImpl;

/**
 *
 * idle robot.
 *
 */
public class IdleRobotImpl implements Idle {

    private final SkinProduct animation;

    /**
     * idle.
     */
    public IdleRobotImpl() {
        super();
        this.animation =  new SkinProductAnimatedImpl(AnimationState.getAtlas(), "idleR", AnimationState.SPEED, PlayMode.LOOP);
        this.animation.setState(AnimationState.IS_IDLE);
    }

    @Override
    public final SkinProduct idle() {
        this.animation.setState(AnimationState.IS_IDLE);
        return animation;
    }



}
