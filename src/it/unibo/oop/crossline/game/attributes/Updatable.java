package it.unibo.oop.crossline.game.attributes;

/**
 * This interface represents an entity that can be updated every world update.
 */
public interface Updatable {

    /**
     * The update method.
     */
    void update();
 
}
