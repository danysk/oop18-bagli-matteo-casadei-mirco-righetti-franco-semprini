package it.unibo.oop.crossline.game.actor.robot;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import it.unibo.oop.crossline.game.attributes.Physical;
import it.unibo.oop.crossline.game.weapon.Weapon;

/**
 * This is the interface used for the robot builder, .
 */
public interface RobotBuilder {

    /**
     * Build the configured robot.
     * @return the robot
     */
    Robot build();

    /**
     * Set the attack range.
     * @param attackRange the attack range
     * @return the robot builder
     */
    RobotBuilder setAttackRange(float attackRange);

    /**
     * Set the robot starting health.
     * @param health the health
     * @return the robot builder
     */
    RobotBuilder setHealth(float health);

    /**
     * Set the robot spawning position.
     * @param position the position
     * @return the robot builder
     */
    RobotBuilder setPosition(Vector2 position);

    /**
     * Set the target that the robot should attack.
     * @param target the target
     * @return the robot builder
     */
    RobotBuilder setTarget(Physical target);

    /**
     * Set the robot's team.
     * @param team the team
     * @return the robot builder
     */
    RobotBuilder setTeam(int team);

    /**
     * Set the robot's weapon.
     * @param weapon the weapon
     * @return the robot builder
     */
    RobotBuilder setWeapon(Weapon weapon);

    /**
     * Set the world where the robot should spawn.
     * @param world the world
     * @return the robot builder
     */
    RobotBuilder setWorld(World world);

}
