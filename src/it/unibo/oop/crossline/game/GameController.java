package it.unibo.oop.crossline.game;

import com.badlogic.gdx.Screen;
/**
 * GameController interface for the game.
 */
public interface GameController extends Screen {
    /**
     * Getter of the view.
     * @return the view
     */
    GameView getView();
    /**
     * Getter of the model.
     * @return the model
     */
    GameModel getModel();

}
