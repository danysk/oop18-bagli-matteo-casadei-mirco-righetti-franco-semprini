package it.unibo.oop.crossline.game.weapon;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;

import it.unibo.oop.crossline.game.actor.Actor;
import it.unibo.oop.crossline.game.bullet.BulletBuilder;

/**
 * This class is the implementation of a weapon, used by an
 * {@link it.unibo.oop.crossline.game.actor.Actor} to shoot in the game.
 */
public class WeaponImpl implements Weapon {

    private final BulletBuilder bulletBuilder;
    private long lastShot = TimeUtils.millis();
    private Actor owner;
    private final long shotDelay;
    private Vector2 direction = new Vector2(0, 0);

    /**
     * Initialize the weapon.
     * 
     * @param shotDelay     the delay between shots
     * @param bulletbuilder the
     *                      {@link it.unibo.oop.crossline.game.bullet.BulletBuilder}
     *                      used to spawn bullets
     */
    public WeaponImpl(final long shotDelay, final BulletBuilder bulletbuilder) {
        this.shotDelay = shotDelay;
        this.bulletBuilder = bulletbuilder;
    }

    @Override
    public final boolean canShoot() {
        return (TimeUtils.millis() - lastShot) > shotDelay;
    }

    @Override
    public final BulletBuilder getBulletBuilder() {
        return bulletBuilder;
    }

    @Override
    public final Vector2 getDirection() {
        return direction;
    }

    @Override
    public final void setDirection(final Vector2 direction) {
        this.direction = direction;
    }

    @Override
    public final void setOwner(final Actor owner) {
        this.owner = owner;
    }

    @Override
    public final void shoot() {
        final Vector2 ownerPosition = owner.getBody().getPosition().cpy();
        final Vector2 bulletPosition = ownerPosition.add(direction.nor()); // We should get the owner size and use that
                                                                           // to set the correct position
        bulletBuilder.setOwner(owner).setPosition(bulletPosition).setDirection(direction).build();
        lastShot = TimeUtils.millis();
    }

    /**
     * HashCode method.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bulletBuilder == null) ? 0 : bulletBuilder.hashCode());
        result = prime * result + ((direction == null) ? 0 : direction.hashCode());
        result = prime * result + (int) (lastShot ^ (lastShot >>> 32));
        result = prime * result + ((owner == null) ? 0 : owner.hashCode());
        result = prime * result + (int) (shotDelay ^ (shotDelay >>> 32));
        return result;
    }

    /**
     * Equals method.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WeaponImpl other = (WeaponImpl) obj;
        if (bulletBuilder == null) {
            if (other.bulletBuilder != null) {
                return false;
            }
        } else if (!bulletBuilder.equals(other.bulletBuilder)) {
            return false;
        }
        if (direction == null) {
            if (other.direction != null) {
                return false;
            }
        } else if (!direction.equals(other.direction)) {
            return false;
        }
        if (lastShot != other.lastShot) {
            return false;
        }
        if (owner == null) {
            if (other.owner != null) {
                return false;
            }
        } else if (!owner.equals(other.owner)) {
            return false;
        } else if (shotDelay != other.shotDelay) {
            return false;
        }
        return true;
    }

    /**
     * ToString method.
     */
    @Override
    public String toString() {
        return "WeaponImpl [bulletBuilder=" + bulletBuilder + ", lastShot=" + lastShot + ", owner=" + owner
                + ", shotDelay=" + shotDelay + ", direction=" + direction + "]";
    }

}
