package it.unibo.oop.crossline.io;

import java.util.Arrays;
import java.util.stream.Collectors;

import com.badlogic.gdx.math.Vector2;

/**
 * Directions for movement.
 */
public enum MoveDirections {
    /**
     * Forward.
     */
    FORWARD {
        @Override
        public Vector2 getDirection() {
            return new Vector2(0, SPEED);
        }

        @Override
        public void print() {
            System.out.println(this.name());
        }

        @Override
        public String toString() {
            return convertToString(this.name(), this.getDirection());
        }
    },
    /**
     * Left.
     */
    LEFT {
        @Override
        public Vector2 getDirection() {
            return new Vector2(-SPEED, 0);
        }

        @Override
        public void print() {
            System.out.println(this.name());
        }

        @Override
        public String toString() {
            return convertToString(this.name(), this.getDirection());
        }
    },
    /**
     * Backward.
     */
    BACKWARD {
        @Override
        public Vector2 getDirection() {
            return new Vector2(0, -SPEED);
        }

        @Override
        public void print() {
            System.out.println(this.name());
        }

        @Override
        public String toString() {
            return convertToString(this.name(), this.getDirection());
        }
    },
    /**
     * Right.
     */
    RIGHT {
        @Override
        public Vector2 getDirection() {
            return new Vector2(SPEED, 0);
        }

        @Override
        public void print() {
            System.out.println(this.name());
        }

        @Override
        public String toString() {
            return convertToString(this.name(), this.getDirection());
        }
    },
    /**
     * Forward left.
     */
    FORWARD_LEFT {
        @Override
        public Vector2 getDirection() {
            return new Vector2(-SPEED, SPEED);
        }

        @Override
        public void print() {
            System.out.println(this.name());
        }

        @Override
        public String toString() {
            return convertToString(this.name(), this.getDirection());
        }
    },
    /**
     * Forward right.
     */
    FORWARD_RIGHT {
        @Override
        public Vector2 getDirection() {
            return new Vector2(SPEED, SPEED);
        }

        @Override
        public void print() {
            System.out.println(this.name());
        }

        @Override
        public String toString() {
            return convertToString(this.name(), this.getDirection());
        }
    },
    /**
     * Backward left.
     */
    BACKWARD_LEFT {
        @Override
        public Vector2 getDirection() {
            return new Vector2(-SPEED, -SPEED);
        }

        @Override
        public void print() {
            System.out.println(this.name());
        }

        @Override
        public String toString() {
            return convertToString(this.name(), this.getDirection());
        }
    },
    /**
     * Backward right.
     */
    BACKWARD_RIGHT {
        @Override
        public Vector2 getDirection() {
            return new Vector2(SPEED, -SPEED);
        }

        @Override
        public void print() {
            System.out.println(this.name());
        }

        @Override
        public String toString() {
            return convertToString(this.name(), this.getDirection());
        }
    };
    /**
     * The movement speed.
     */
    private static final float SPEED = 3f;

    /**
     * @return the direction of action.
     */
    public abstract Vector2 getDirection();

    /**
     * Print name of action.
     */
    public abstract void print();

    /**
     * Print all move direcions.
     */
    public static void printAll() {
        System.out.println("Move Directions [");
        Arrays.stream(values()).forEach(System.out::println);
        System.out.println("]");
    }

    /**
     * convertToString method for each direction.
     * 
     * @param name the name
     * @param vect the vector
     * @return a string based on name and vector
     */
    private static String convertToString(final String name, final Vector2 vect) {
        return "Move direction [" + "name=" + name + ", direction=" + vect.toString() + "]";
    }

    /**
     * AsString method (like toString one, but for enums).
     * 
     * @return the string of all ements of enum
     */
    public static String asString() {
        return "Move directions "
                + Arrays.stream(values()).map(d -> "\n" + d.toString()).collect(Collectors.toList()).toString();
    }
}
