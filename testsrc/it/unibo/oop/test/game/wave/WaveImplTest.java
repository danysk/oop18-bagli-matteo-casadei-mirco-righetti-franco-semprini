package it.unibo.oop.test.game.wave;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import it.unibo.oop.crossline.game.actor.player.PlayerImpl;
import it.unibo.oop.crossline.game.actor.robot.RobotImpl;
import it.unibo.oop.crossline.game.bullet.BulletBuilder;
import it.unibo.oop.crossline.game.bullet.BulletBuilderImpl;
import it.unibo.oop.crossline.game.wave.WaveImpl;
import it.unibo.oop.crossline.game.weapon.WeaponImpl;

/**
 * Test class for WaveImpl.
 */
public class WaveImplTest {

    /**
     * Zero value.
     */
    private static final int ZERO = 0;
    /**
     * One value.
     */
    private static final int ONE = 1;
    /**
     * First wave.
     */
    private WaveImpl wave1;
    /**
     * Second wave.
     */
    private WaveImpl wave2;
    /**
     * Same as first wave.
     */
    private WaveImpl sameAsWave1;
    /**
     * Unimplemented wave.
     */
    private static final WaveImpl UNIMPLEMENTED_WAVE = null;
    private static final float DIFFICULTY1 = 0;
    private static final float DIFFICULTY2 = 2;
    private static final float EARTH_GRAVITY = -9.8f;
    private static final double HEALTH_1 = 50.0f;
    private static final double HEALTH_2 = 100.0f;
    private static final RobotImpl UNIMPLEMENTED_ROBOT = null;
    private static final long SHOT_DELAY1 = 3000;
    private static final long SHOT_DELAY2 = 2000;
    private static final float BASE_BULLET_DAMAGE = 10f;
    private static final float BASE_BULLET_SPEED = 2f;
    private World world;

    /**
     * SetUp method for instance waves.
     * 
     * @throws java.lang.Exception generic exception
     */
    @Before
    public void setUp() throws Exception {
        world = new World(new Vector2(0, EARTH_GRAVITY), true);
        final PlayerImpl player1 = new PlayerImpl(world, new Vector2(1, 0));
        final PlayerImpl player2 = new PlayerImpl(world, new Vector2(0, 1));
        wave1 = new WaveImpl(player1, DIFFICULTY1);
        wave2 = new WaveImpl(player2, DIFFICULTY2);
        sameAsWave1 = wave1;
    }

    /**
     * Test method for {@link it.unibo.oop.crossline.game.wave.WaveImpl#hashCode()}.
     */
    @Test
    public void testHashCode() {
        assertNotEquals(wave1.hashCode(), ZERO);
        assertNotEquals(wave2.hashCode(), ZERO);
        assertNotEquals(wave1.hashCode(), wave2.hashCode());
        assertEquals("wave1 equals to sameAsWave1", wave1.hashCode(), sameAsWave1.hashCode());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.wave.WaveImpl#WaveImpl(it.unibo.oop.crossline.game.actor.player.Player, float)}.
     */
    @Test
    public final void testWaveImpl() {
        assertNotNull("wave1 is not null", wave1);
        assertNotNull("wave2 is not null", wave2);
        assertNotNull("sameAsWave1 is not null", sameAsWave1);
        assertNull("UNIMPLEMENTED_WAVE is null", UNIMPLEMENTED_WAVE);
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.wave.WaveImpl#update(java.util.Observable, java.lang.Object)}.
     */
    @Test
    public final void testUpdate() {
        final BulletBuilder bulletBuilder = new BulletBuilderImpl().setDamage(BASE_BULLET_DAMAGE * DIFFICULTY1)
                .setSpeed(BASE_BULLET_SPEED * DIFFICULTY1);
        final WeaponImpl weapon1 = new WeaponImpl(SHOT_DELAY1, bulletBuilder);
        final WeaponImpl weapon2 = new WeaponImpl(SHOT_DELAY2, bulletBuilder);

        final RobotImpl robot1 = new RobotImpl(ZERO, (float) HEALTH_1, new Vector2(1, 0), UNIMPLEMENTED_ROBOT, ZERO,
                weapon1, world);
        final RobotImpl robot2 = new RobotImpl(10, (float) HEALTH_2, new Vector2(0, 1), robot1, ONE, weapon2, world);
        /*
         * With the difficulty at 0 there are no robots for example with difficulty 5,
         * there are 5*1.5 = 7 robots
         */
        assertEquals("the robot list of wave1 has no robots", wave1.getRobots().size(), ZERO);

        wave1.getRobots().add(robot1);
        wave1.getRobots().add(robot2);
        assertNotNull("the robot list of wave1 is not null", wave1.getRobots());
        assertEquals("the robot list of wave1 has 2 robots", wave1.getRobots().size(), 2);
        wave1.update(robot1, null);
        assertEquals("the robot list of wave1 has 1 robots", wave1.getRobots().size(), 1);
        wave1.update(robot2, null);
        assertEquals("the robot list of wave1 has no robots", wave1.getRobots().size(), 0);
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.wave.WaveImpl#getRobots()}.
     */
    @Test
    public final void testGetRobots() {
        assertNotNull("the robot list of wave1 is not null", wave1.getRobots());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.wave.WaveImpl#getDifficulty()}.
     */
    @Test
    public final void testGetDifficulty() {
        assertNotNull("the robot list of wave1 is not null", wave1.getRobots());
    }

    /**
     * Test method for {@link it.unibo.oop.crossline.game.wave.WaveImpl#hasEnded()}.
     */
    @Test
    public final void testHasEnded() {
        assertTrue("the wave1 has ended", wave1.hasEnded());
    }

    /**
     * Test method for {@link it.unibo.oop.crossline.game.wave.WaveImpl#toString()}.
     */
    @Test
    public void testToString() {
        assertNotNull("the string of wave1 is not null", wave1.toString());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.wave.WaveImpl#equals(java.lang.Object)}.
     */
    @Test
    public void testEqualsObject() {
        assertNotEquals(wave1, wave2);
        assertEquals("wave1 equals to sameAsWave1", wave1, sameAsWave1);
    }
}
