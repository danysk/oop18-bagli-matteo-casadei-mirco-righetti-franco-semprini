package it.unibo.oop.test.game.world.spike;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import it.unibo.oop.crossline.game.world.spike.SpikeImpl;

/**
 * Test class for SpikeImpl.
 */
public class SpikeImplTest {

    /**
     * Zero value.
     */
    private static final int ZERO = 0;
    /**
     * First spike.
     */
    private SpikeImpl spike1;
    /**
     * Second spike.
     */
    private SpikeImpl spike2;
    /**
     * Same as first spike.
     */
    private SpikeImpl sameAsSpike1;
    /**
     * Unimplemented spike.
     */
    private static final SpikeImpl UNIMPLEMENTED_SPIKE = null;
    private static final float EARTH_GRAVITY = -9.8f;

    /**
     * SetUp method for instance spikes.
     * 
     * @throws java.lang.Exception generic exception
     */
    @Before
    public void setUp() throws Exception {
        final World world = new World(new Vector2(0, EARTH_GRAVITY), true);
        spike1 = new SpikeImpl(world, new Vector2(1, 0));
        spike2 = new SpikeImpl(world, new Vector2(0, 1));
        sameAsSpike1 = spike1;
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.world.spike.SpikeImpl#hashCode()}.
     */
    @Test
    public final void testHashCode() {
        assertNotEquals(spike1.hashCode(), ZERO);
        assertNotEquals(spike2.hashCode(), ZERO);
        assertNotEquals(spike1.hashCode(), spike2.hashCode());
        assertEquals("spike1 equals to sameAsSpike1", spike1.hashCode(), sameAsSpike1.hashCode());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.world.spike.SpikeImpl#SpikeImpl(com.badlogic.gdx.physics.box2d.World, com.badlogic.gdx.math.Vector2)}.
     */
    @Test
    public final void testSpikeImpl() {
        assertNotNull("spike1 is not null", spike1);
        assertNotNull("spike2 is not null", spike2);
        assertNotNull("sameAsSpike1 is not null", sameAsSpike1);
        assertNull("UNIMPLEMENTED_SPIKE is null", UNIMPLEMENTED_SPIKE);
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.world.spike.SpikeImpl#getBody()}.
     */
    @Test
    public final void testGetBody() {
        assertNotNull("the body of spike1 is not null", spike1.getBody());
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.world.spike.SpikeImpl#equals(java.lang.Object)}.
     */
    @Test
    public final void testEqualsObject() {
        assertNotEquals(spike1, spike2);
        assertEquals("spike1 equals to sameAsSpike1", spike1, sameAsSpike1);
    }

    /**
     * Test method for
     * {@link it.unibo.oop.crossline.game.world.spike.SpikeImpl#toString()}.
     */
    @Test
    public final void testToString() {
        assertNotNull("the string of spike1 is not null", spike1.toString());
    }

}
