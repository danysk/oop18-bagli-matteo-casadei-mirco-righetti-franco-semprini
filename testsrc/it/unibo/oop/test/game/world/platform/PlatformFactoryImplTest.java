package it.unibo.oop.test.game.world.platform;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import it.unibo.oop.crossline.game.world.platform.PlatformFactory;
import it.unibo.oop.crossline.game.world.platform.PlatformFactoryImpl;
import it.unibo.oop.crossline.game.world.platform.PlatformImpl;

/**
 * Test class for PlatformFactoryImpl.
 */
public class PlatformFactoryImplTest {

    private static final float EARTH_GRAVITY = -9.8f;

    /**
     * Test the platformFactory correct usage.
     */
    @Test
    public final void test() {
        final World world = new World(new Vector2(0, EARTH_GRAVITY), true);
        final PlatformImpl platform1 = new PlatformImpl(world, new Vector2(1, 0));
        final PlatformFactory platformFactory = new PlatformFactoryImpl(world);
        final PlatformImpl platform2 = (PlatformImpl) platformFactory.createPlatform(new Vector2(1, 0));
        assertEquals("platformFactory has created a platform with position equals to platform1's position", 
                platform2.getPosition(), platform1.getPosition());
    }

}
